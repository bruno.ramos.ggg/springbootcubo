package introducao.controller;

import introducao.dto.PetDto;
import introducao.form.PetForm;
import introducao.model_entitys.Pet;
import introducao.model_entitys.Tutor;
import introducao.service.PetService;
import introducao.service.TutorService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Objects;
import java.util.Optional;

//@Component -> Geral (Service, Controller, Repository ...)
//@Controller -> Necessida do @ResponseBody nos Métodos
@RestController
@RequestMapping("/api/v1/pets")
public class PetController {

    @Autowired
    PetService petService;

    @Autowired
    TutorService tutorService;

    @GetMapping
    @Cacheable(value = "listaPets")
    public Page<PetDto> getPets(String animal, @PageableDefault(page = 0, size = 2, sort = "nome", direction = Sort.Direction.ASC) Pageable paginacao) {

        System.out.println("Passei por aqui");
//        size -> tamanho da página
//        page -> número da página que você quer
//        sort -> Ordenação por algum

        if (animal != null) {
            return PetDto.converter(petService.searchPetByAnimal(animal, paginacao));
        }

        return PetDto.converter(petService.getPets(paginacao));
    }

//    @GetMapping("/filter/tutor")
//    public Page<PetDto> getPetsTutor(String nomeTutor, @PageableDefault(page = 0, size = 2, sort = "nome", direction = Sort.Direction.ASC) Pageable paginacao) {
//
//        if (nomeTutor != null) {
//            return PetDto.converter(petService.searchPetByNomeTutor(nomeTutor, paginacao));
//        }
//
//        return PetDto.converter(petService.getPets(paginacao));
//    }

    @PostMapping
    @Transactional
    @CacheEvict(value = "listaPets", allEntries = true)
    public ResponseEntity<Object> savePet(@RequestBody @Valid PetForm petForm) {

        Optional<Tutor> optionalTutor = tutorService.findByNomeOptional(petForm.getNomeTutor());

        if (!optionalTutor.isPresent()) {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body("O tutor não existe, não foi cadastrar os Pet!");
        }

        Optional<Pet> optionalPet = petService.getPet(petForm.getNome());

        if (!optionalPet.isPresent()) {
            Pet pet = petForm.converte(tutorService);

            return ResponseEntity
                    .status(HttpStatus.CREATED)
                    .body(petService.savePet(pet));

        } else if (!(Objects.equals(optionalPet.get().getTutor().getNome(), petForm.getNomeTutor()))) {
            Pet pet = petForm.converte(tutorService);

            return ResponseEntity
                    .status(HttpStatus.CREATED)
                    .body(petService.savePet(pet));
        }

        return ResponseEntity
                .status(HttpStatus.CONFLICT)
                .body("Olá, o pet já esta cadastrado/O tutor não existe!");
    }

    @DeleteMapping("/{id}")
    @CacheEvict(value = "listaPets", allEntries = true)
    @Transactional
    public ResponseEntity<Object> deletePet(@PathVariable Long id) {
        Optional<Pet> petOptional = petService.getPetById(id);

        if (petOptional.isPresent()) {
            petService.deletePet(id);
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body("Pet deletado com successo!");
        }

        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body("Pet não encontrado!");
    }

    @PutMapping("/{id}")
    @CacheEvict(value = "listaPets", allEntries = true)
    @Transactional
    public ResponseEntity<Object> atualizaPet(@PathVariable Long id, @RequestBody @Valid PetForm petForm) {

        Optional<Pet> optionalPet = petService.getPetById(id);

        if (!optionalPet.isPresent()) {
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body("Pet não cadastrado!");
        }

        Pet pet = new Pet();
        BeanUtils.copyProperties(petForm, pet);
        pet.setTutor(optionalPet.get().getTutor());
        pet.setId(optionalPet.get().getId());

//        Pet pet = optionalPet.get();
//        pet.setNome(petForm.getNome());
//        pet.setRaca(petForm.getRaca());
//        pet.setAnimal(petForm.getAnimal());

        return ResponseEntity
                .status(HttpStatus.OK)
                .body(petService.savePet(pet));
    }

//    put -> Atualizar TUDO
//    patch -> Atualizar somento uma ou algumas propriedades da informação
}