package introducao.dto;

import introducao.model_entitys.Pet;
import introducao.model_entitys.Tutor;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.stream.Collectors;

public class PetDto {

    private String nome;
    private String animal;
    private String raca;
    private Tutor tutor;

    public PetDto(Pet pet) {
        this.nome = pet.getNome();
        this.animal = pet.getAnimal();
        this.raca = pet.getRaca();
        this.tutor = pet.getTutor();
    }

    public static Page<PetDto> converter(Page<Pet> listPet) {

//        List<PetDto> listPetDto = new ArrayList<>();
//
//        for (int i = 0; i < pet.size(); i++) {
//
//            PetDto petDto = new PetDto(pet.get(i));
//
//            listPetDto.add(petDto);
//
//        }

        // stream() -> Transforma uma lista em um conjunto;
        // map(function) -> Percorre uma lista/conjunto e aplica a função passada como parâmetro;
        // collect(Método) -> Pega uma lista/conjunto e cria algo passado como método;
        return listPet.map(PetDto::new);

    }

    public String getNome() {
        return nome;
    }

    public String getAnimal() {
        return animal;
    }

    public String getRaca() {
        return raca;
    }

    public Tutor getTutor() {
        return tutor;
    }
}
