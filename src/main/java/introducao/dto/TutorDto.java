package introducao.dto;

import introducao.model_entitys.Tutor;

public class TutorDto {

    private String nome;
    private String contato;

    public TutorDto(Tutor tutorJaCriado) {
        this.nome = tutorJaCriado.getNome();
        this.contato = tutorJaCriado.getContato();
    }

    public String getNome() {
        return nome;
    }

    public String getContato() {
        return contato;
    }
}
