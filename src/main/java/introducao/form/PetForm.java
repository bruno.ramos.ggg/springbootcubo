package introducao.form;

import introducao.model_entitys.Pet;
import introducao.model_entitys.Tutor;
import introducao.service.TutorService;
import org.hibernate.validator.constraints.Length;
import org.springframework.http.ResponseEntity;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class PetForm {

    @NotEmpty @NotNull @Length(min = 2)
    private String nome;
    @NotEmpty @NotNull @Length(min = 2, max = 140)
    private String animal;
    @NotEmpty @NotNull @Length(min = 2, max = 140)
    private String raca;
    @NotEmpty @NotNull @Length(min = 2)
    private String nomeTutor;

    public Pet converte(TutorService tutorService) {

        Tutor tutor = tutorService.findByNome(nomeTutor);

        return new Pet(
                nome,
                animal,
                raca,
                tutor
        );
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getAnimal() {
        return animal;
    }

    public void setAnimal(String animal) {
        this.animal = animal;
    }

    public String getRaca() {
        return raca;
    }

    public void setRaca(String raca) {
        this.raca = raca;
    }

    public String getNomeTutor() {
        return nomeTutor;
    }

    public void setNomeTutor(String nomeTutor) {
        this.nomeTutor = nomeTutor;
    }
}
