package introducao.service;

import introducao.model_entitys.Tutor;
import introducao.repository.TutorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class TutorService {

    @Autowired
    TutorRepository tutorRepository;

    public Tutor findByNome(String nomeTutor) {
        return tutorRepository.findByNome(nomeTutor);
    }

    public Optional<Tutor> findByNomeOptional(String nomeTutor) {
        return Optional.ofNullable(tutorRepository.findByNome(nomeTutor));
    }
}
