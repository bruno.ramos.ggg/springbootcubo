package introducao.service;

import introducao.model_entitys.Pet;
import introducao.repository.PetsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PetService {

    // @Autowired -> Implementa o IoC e Instancia automaticamente o PetsRepository na classe de Servico (PetService)
    @Autowired
    PetsRepository petsRepository;

    public Page<Pet> getPets(Pageable paginacao) {
        return petsRepository.findAll(paginacao);
    }

    public Page<Pet> searchPetByAnimal(String animal, Pageable paginacao) {
        return petsRepository.findByAnimal(animal, paginacao);
    }

//    public Page<Pet> searchPetByNomeTutor(String nomeTutor, Pageable paginacao) {
//        return petsRepository.findByTutor_Nome(nomeTutor, paginacao);
//    }

    public Pet savePet(Pet pet) {
        return petsRepository.save(pet);
    }

    public Optional<Pet> getPet(String nome) {
        return petsRepository.findByNome(nome);
    }

    public Optional<Pet> getPetById(Long id) {
        return petsRepository.findById(id);
    }

    public void deletePet(Long id) {
        petsRepository.deleteById(id);
    }
}