package introducao.repository;

import introducao.model_entitys.Pet;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

// JpaRepository -> Tem uma tunha de métodos de comunicação com o banco.
public interface PetsRepository extends JpaRepository<Pet, Long> {
    Page<Pet> findByAnimal(String animal, Pageable paginacao);

//    Page<Pet> findByTutor_Nome(String nomeTutor, Pageable paginacao);

    Optional<Pet> findByNome(String nome);

//    Caso queria utilizar uma Query SQL em especifico
//    @Query("SELECT ....")
//    Optional<Pet> findByNomeWithQuery(String nome);

    //Cria conexão com o banco
    //Cria um cursor
    //Executa um SQL que eu mandar
    //Retorna as informações
    //Fechar tanto o cursor quando a conexão
}
