package introducao.repository;

import introducao.model_entitys.Tutor;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface TutorRepository extends JpaRepository<Tutor, Long> {
    Tutor findByNome(String nomeTutor);
}