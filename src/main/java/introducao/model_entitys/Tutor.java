package introducao.model_entitys;

import javax.persistence.*;

@Entity
@Table(name = "TUTOR_TABLE")
public class Tutor {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(length = 150, nullable = false)
    private String nome;
    @Column(nullable = false)
    private String contato;

    public Tutor(String nome, String contato) {
        this.nome = nome;
        this.contato = contato;
    }

    public Tutor() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getContato() {
        return contato;
    }

    public void setContato(String contato) {
        this.contato = contato;
    }
}
