package introducao.model_entitys;

import javax.persistence.*;

@Entity
@Table(name = "PET_TABLE")
public class Pet {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    //NOT NULL -> nullable = false
    //length -> VARCHAR(X)
    @Column(length = 70, nullable = false)
    private String nome;
    @Column(nullable = false)
    private String animal;
    @Column(nullable = false)
    private String raca;

    @ManyToOne
    private Tutor tutor;

    public Pet(String nome, String animal, String raca, Tutor tutor) {
        this.nome = nome;
        this.animal = animal;
        this.raca = raca;
        this.tutor = tutor;
    }

    public Pet() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getAnimal() {
        return animal;
    }

    public void setAnimal(String animal) {
        this.animal = animal;
    }

    public String getRaca() {
        return raca;
    }

    public void setRaca(String raca) {
        this.raca = raca;
    }

    public Tutor getTutor() {
        return tutor;
    }

    public void setTutor(Tutor tutor) {
        this.tutor = tutor;
    }
}
