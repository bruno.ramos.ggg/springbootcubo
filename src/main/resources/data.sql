INSERT INTO TUTOR_TABLE (nome, contato) VALUES ('Bruno', 'bruno.ramos@mv.com.br');
INSERT INTO TUTOR_TABLE (nome, contato) VALUES ('Juvenal', 'juvenal.souza@mv.com.br');

INSERT INTO PET_TABLE (nome, animal, raca, tutor_id) VALUES ('Creiton', 'Cachorro', 'NDA', 1);
INSERT INTO PET_TABLE (nome, animal, raca, tutor_id) VALUES ('Apolo', 'Gato', 'NDA', 1);
INSERT INTO PET_TABLE (nome, animal, raca, tutor_id) VALUES ('Toto', 'Cachorro', 'NDA', 2);
INSERT INTO PET_TABLE (nome, animal, raca, tutor_id) VALUES ('Aghata', 'Cachorro', 'NDA', 2);
INSERT INTO PET_TABLE (nome, animal, raca, tutor_id) VALUES ('Freud', 'Gato', 'NDA', 2);